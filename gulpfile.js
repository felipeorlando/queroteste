const gulp = require('gulp')
const path = require('path')
const sass = require('gulp-sass')
const autoprefixer = require('gulp-autoprefixer')
const rename = require('gulp-rename')
const csso = require('gulp-csso')
const htmlreplace = require('gulp-html-replace')
const imagemin = require('gulp-imagemin')
const browserSync = require('browser-sync').create()
const runSequence = require('run-sequence')

const SASS_INCLUDE_PATHS = [
  path.join(__dirname, 'node_modules', 'normalize.css'),
  path.join(__dirname, 'node_modules', 'tetris-grid', 'dist'),
]

gulp.task('sass', () => {
  return gulp.src('./src/scss/main.scss')
    .pipe(sass({ includePaths: SASS_INCLUDE_PATHS }).on('error', sass.logError))
    .pipe(autoprefixer({ browsers: ['last 2 versions'] }))
    .pipe(gulp.dest('./public'))
})

gulp.task('sass:prod', () => {
  return gulp.src('./public/main.css')
    .pipe(csso())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('./public'))
})

gulp.task('html', () => {
  return gulp.src('./src/index.html')
    .pipe(gulp.dest('./public'))
})

gulp.task('html:prod', () => {
  return gulp.src('./src/index.html')
    .pipe(htmlreplace({ 'css': 'main.min.css' }))
    .pipe(gulp.dest('./public'))
})

gulp.task('images', () => {
  return gulp.src('./src/images/**/*')
    .pipe(imagemin())
    .pipe(gulp.dest('./public/images'))
})

gulp.task('watch', () => {
  const reload = browserSync.reload

  browserSync.init({
    server: './public'
  })

  gulp.watch('./src/scss/**/*.scss')
    .on('change', () => gulp.start('sass', reload))

  gulp.watch('./src/index.html')
    .on('change', () => gulp.start('html', reload))

  gulp.watch('./src/images/**/*')
    .on('change', () => gulp.start('images', reload))
})

gulp.task('default', () => {
  runSequence('images', 'html', 'sass', 'watch')
})

gulp.task('build', () => {
  runSequence('html:prod', 'sass', 'sass:prod')
})
